'use strict';

$(document).ready(function () {
    // Mobile menu
    function openMobMenu() {
        let btn = $('#mob-menu');
        let menu = $('#header');

        btn.on('click', function () {
            $(this).toggleClass('mob-menu_active');
            menu.toggleClass('header_active');
        });
    }

    openMobMenu();

    // PopUp
    function openPopUp() {
        $('.js-popup-button').on('click', function () {
            $('.popup').removeClass('js-popup-show');
            let popupClass = '.' + $(this).attr('data-popupShow');
            $(popupClass).addClass('js-popup-show');
            if ($(document).height() > $(window).height()) {
                let scrollTop = ($('html').scrollTop()) ? $('html').scrollTop() : $('body').scrollTop();
                $('html').addClass('noscroll').css('top', -scrollTop);
            }
        });
        closePopup();
    }

    // Close PopUp
    function closePopup() {
        $('.js-close-popup').on('click', function () {
            $('.popup').removeClass('js-popup-show');
            let scrollTop = parseInt($('html').css('top'));
            $('html').removeClass('noscroll');
            $('html, body').scrollTop(-scrollTop);
            return false;
        });

        $('.popup').on('click', function (e) {
            let div = $('.popup__wrap');

            if (!div.is(e.target) && div.has(e.target).length === 0) {
                $('.popup').removeClass('js-popup-show');
                let scrollTop = parseInt($('html').css('top'));
                $('html').removeClass('noscroll');
                $('html, body').scrollTop(-scrollTop);
            }
        });
    }

    openPopUp();

    // Scroll
    $('#personal-info').find('.table-wrap__inner').scrollbar();
    $('#main').find('.winners-table__wrap').scrollbar();

    // Masked Phone
    $('input[type="tel"]').mask('+7(999)999-99-99');

    // Tabs
    function openTabs() {
        let wrap = $('#personal-info');
        let tabsContent = wrap.find('.table-wrap');
        let tabsBtn = wrap.find('.tabs li');

        // Reset
        tabsBtn.eq(0).addClass('active');
        tabsContent.eq(0).addClass('active');

        // Tabs Main Action
        wrap.find('.tabs').on('click', 'li', function () {
            let i = $(this).index();

            tabsBtn.removeClass('active');
            $(this).addClass('active');
            tabsContent.removeClass('active');
            tabsContent.eq(i).addClass('active');
        });
    }

    openTabs();

    // Select
    $('select').select2();

    // Jquery Validate
    function checkValidate() {
        let form = $('form');

        $.each(form, function () {
            $(this).validate({
                ignore: [],
                errorClass: 'error',
                validClass: 'success',
                rules: {
                    name: {
                        required: true,
                        letters: true
                    },
                    surname: {
                        required: true,
                        letters: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    phone: {
                        required: true,
                        phone: true
                    },
                    city: {
                        required: true,
                        letters: true
                    },
                    captcha: {
                        required: true,
                        normalizer: function (value) {
                            return $.trim(value);
                        }
                    },
                    password: {
                        required: true,
                        normalizer: function (value) {
                            return $.trim(value);
                        }
                    },
                    home: {
                        required: true,
                        normalizer: function (value) {
                            return $.trim(value);
                        }
                    },
                    apartment: {
                        required: true,
                        normalizer: function (value) {
                            return $.trim(value);
                        }
                    },
                    textarea: {
                        required: true,
                        normalizer: function (value) {
                            return $.trim(value);
                        }
                    }
                },
                messages: {
                    phone: 'Некорректный номер',
                    rules: {
                        required: ''
                    },
                    personal: {
                        required: ''
                    }
                },
            });

            jQuery.validator.addMethod('letters', function (value, element) {
                return this.optional(element) || /^([a-zа-яё]+)$/i.test(value);
            });

            jQuery.validator.addMethod('email', function (value, element) {
                return this.optional(element) || /\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}/.test(value);
            });

            jQuery.validator.addMethod('phone', function (value, element) {
                return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
            });

        });

    }

    checkValidate();

    // Define mobile
    function defineMob() {
        let wrap = $('#header').find('.discription-att');
        let define = detect.parse(navigator.userAgent);
        let os = define.os.family;

        if (os == 'iOS') {
            wrap.addClass('discription-att_ios');
        } else if (os == 'Android') {
            wrap.addClass('discription-att_android');
        }
    }

    defineMob();

});